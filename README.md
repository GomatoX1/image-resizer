# Image resizer 🎆

Simple image resizer

- Add your images to original folder in root
- Run `yarn resize`
- Only resized will appear in resized folder and optimized and resized in optimized folder
