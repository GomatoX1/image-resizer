import thumbnail from 'node-thumbnail';
import optimize from './src/optimize.js';
import ask from './src/ask.js';
import { prepareFolder, cleanDirectory } from './src/clean.js';
import chalk from 'chalk';

const log = console.log;

async function resize() {
  await prepareFolder('./original').catch((error) => {
    log(chalk.red(error));
  });

  await cleanDirectory('./resized').catch((error) => {
    log(chalk.red(error));
  });

  await cleanDirectory('./optimized').catch((error) => {
    log(chalk.red(error));
  });

  const result = await ask();

  thumbnail.thumb(
    {
      source: './original', // could be a filename: dest/path/image.jpg
      destination: './resized',
      suffix: '',
      width: result.size || 300,
    },
    function (files, err, stdout, stderr) {
      optimize(
        {
          source: './resized',
          output: './optimized',
        },
        1000
      )
        .catch((error) => {
          log(chalk.red(error));
        })
        .then(() => {
          log(chalk.green('All done! 🚀'));
        });
    }
  );
}

(async () => {
  await resize();
})();
