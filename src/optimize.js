import compress from 'compress-images';

function optimize(config) {
  return new Promise((resolve, reject) => {
    compress(
      `${config.source}/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}`,
      `${config.output}/`,
      { compress_force: false, statistic: true, autoupdate: true },
      false,
      { jpg: { engine: 'mozjpeg', command: ['-quality', '60'] } },
      { png: { engine: 'pngquant', command: ['--quality=20-50', '-o'] } },
      { svg: { engine: 'svgo', command: '--multipass' } },
      {
        gif: { engine: 'gifsicle', command: ['--colors', '64', '--use-col=web'] },
      },
      function (error, completed) {
        if (completed === true) {
          resolve();
        }

        if (error) {
          reject(error);
        }
      }
    );
  });
}

export default optimize;
