import fs from 'fs';

const cleanDirectory = (directory) => {
  return new Promise((resolve, reject) => {
    fs.rmdir(directory, { recursive: true }, (error) => {
      if (error) {
        reject(error);
      } else {
        try {
          fs.mkdirSync(directory);
        } catch (e) {
          if (e.code != 'EEXIST') {
            throw e;
          }
        }

        resolve();
      }
    });
  });
};

const prepareFolder = (directory) => {
  return new Promise((resolve, reject) => {
    fs.unlink(`${directory}/.DS_Store`, function (error) {
      if (error) {
        if (error.code === 'ENOENT') {
          resolve();
        } else {
          reject(error);
        }
      } else {
        resolve();
      }
    });
  });
};

export { cleanDirectory, prepareFolder };
