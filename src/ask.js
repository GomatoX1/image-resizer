import prompt from 'prompt';
import chalk from 'chalk';

const log = console.log;

function ask() {
  return new Promise((resolve, reject) => {
    prompt.start();

    log(chalk.bgWhite.black('Enter image 🌆  size (default: 300)'));

    prompt.get(
      [
        {
          name: 'size',
          description: 'Size',
          type: 'integer',
          required: true,
        },
      ],
      function (err, result) {
        if (err) {
          return onErr(err);
        }

        resolve(result);
      }
    );
  });
}

function onErr(err) {
  console.log(err);
  return 1;
}

export default ask;
